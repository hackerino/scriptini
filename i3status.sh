#!/bin/sh
i3status | while :
do
        read line
	
	# 1. Ottengo la canzone attuale. 
	# 2. Rimuovo il percorso e lascio solo il nome del file, oppure se non è un percorso non fa nulla.
	# 3. Se mpc current fallisce per qualche motivo esce e la barra non funziona

	playing=$(mpc current | sed "s/.*\///" || exit 1)

	# Per qualche stupido motivo se non lo metto nell'else continua a lampeggiare l'antescritta.

        if [ "$playing" ]; then
		echo -n "$playing | $line" || exit 1
	else
		echo -n "$line"
	fi

done
